#!/bin/bash

echo "* * * * * root date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z"" >> /etc/crontab;
wget https://binaries.cockroachdb.com/cockroach-v20.2.4.linux-amd64.tgz;
tar xzf cockroach-v20.2.4.linux-amd64.tgz;
cp cockroach-v20.2.4.linux-amd64/cockroach /usr/local/bin;
mkdir /bfdata;
git clone https://gitlab.com/kmarstrander/lidey.git;
cd lidey/server_setup/;
cockroach start --max-offset=1500ms --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=192.168.130.90:26257,192.168.128.117:26257,192.168.131.2:26257 --advertise-addr=192.168.130.90:26257;
cockroach init --insecure --host=192.168.130.90:26257;
echo "*/5 * * * * root date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z"" >> /etc/crontab;
cp bookface.service /etc/systemd/system/bookface.service;
cp bookface_start_server1.sh /root/bookface_start.sh;
chmod +x /root/bookface_start.sh;
systemctl enable bookface;

