#!/bin/bash

echo "Bookface auto start script";

$current_host=hostname -i | awk '{print $1}';

cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=10.212.142.52:26257,10.212.141.187:26257,10.212.141.221:26257 --advertise-addr=$current_host:26257;

chmod +x /root/bookface_start.sh;
systemctl enable bookface;
