#!/bin/bash

sudo wget https://binaries.cockroachdb.com/cockroach-v20.2.4.linux-amd64.tgz ;
sudo tar xzf cockroach-v20.2.4.linux-amd64.tgz ;
sudo cp cockroach-v20.2.4.linux-amd64/cockroach /usr/local/bin ;
sudo mkdir /bfdata;

current_host = $(hostname -i | awk '{print $1}');

sudo cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=10.212.142.52:26257,10.212.141.187:26257,10.212.141.221:26257 --advertise-addr=$current_host:26257;

sudo echo "*/5 * * * * root date -s "$(sudo wget -qSO- --max-redirect=0 google.com 2>&1 | sudo grep Date: | sudo cut -d' ' -f5-8)Z"" >>/etc/crontab;
