#!/bin/bash

sudo apt-get remove docker docker-engine docker.io containerd runc 2>/dev/null;

sudo apt-get purge docker-ce docker-ce-cli containerd.io 2>/dev/null;

sudo rm -rf /var/lib/docker 2>/dev/null;

sudo apt-get update;

sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common;

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable";

sudo apt-get update;
sudo apt-get install -y docker-ce docker-ce-cli containerd.io;

sudo chmod +x docker_image_start.sh;

sudo ./docker_image_start.sh;

sudo cp -r ./code/* /var/lib/docker/volumes/src/_data/;
