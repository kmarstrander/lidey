#!/bin/bash

sudo docker volume create src;

sudo docker build -t www1 -f- .< Dockerfile;
sudo docker build -t www2 -f- .< Dockerfile;

sudo docker run -d -it -p 31233:80 --name=www1 --mount source=src,destination=/bookface www1;
sudo docker run -d -it -p 31500:80 --name=www2 --mount source=src,destination=/bookface www2;
sudo docker run --name=memcache -p 11211:11211 -d memcached memcached -m 128;


sudo docker exec -it www1 bash -c '/etc/init.d/apache2 start';
sudo docker exec -it www2 bash -c '/etc/init.d/apache2 start';


